﻿using System;
using System.IO;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Snake
{
    class MainMenu
    {
        private int cursor = 0;
        private bool menuLoop = true;
        private ConsoleKeyInfo ck;
        private FileStream fs;
        public static string weather;
        public static bool weatherModeEnable = true;

        public void Run()
        {
            Console.CursorVisible = false;

            Console.WriteLine("Loading");
            try
            {
                WebRequest wReq = WebRequest.Create("https://query.yahooapis.com/v1/public/yql?q=select%20item.condition.text%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22Buenos%20Aires%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys");
                WebResponse wRta = wReq.GetResponse();

                Stream strm = wRta.GetResponseStream();
                StreamReader strRead = new StreamReader(strm);

                JObject jObj = JObject.Parse(strRead.ReadToEnd());
                weather = (string)jObj.SelectToken("query.results.channel.item.condition.text");

                strm.Close();
                strRead.Close();
            }
            catch (WebException)
            {
                weatherModeEnable = false;
            }


            Console.Clear();

            if (File.Exists("WelcomeMessage.txt"))
            {
                fs = File.OpenRead("WelcomeMessage.txt");
                StreamReader sr = new StreamReader(fs);
                Console.SetCursorPosition(50, 10);
                Console.WriteLine(sr.ReadLine());
                Console.ReadKey();
                sr.Close();
                fs.Close();            
            }
            else
            {
                fs = File.Create("WelcomeMessage.txt");
                StreamWriter sw = new StreamWriter(fs);
                Console.SetCursorPosition(50, 9);
                Console.WriteLine("Ingrese un mensaje de bienvenida:");
                Console.SetCursorPosition(50, 10);
                sw.WriteLine(Console.ReadLine());
                sw.Close();
                fs.Close();
            }

            ck = new ConsoleKeyInfo();

            while (menuLoop)
            {
                InputMannager();
                DrawScreen();
                System.Threading.Thread.Sleep(10);            
            }
        }

        private void InputMannager()
        {
            if (Console.KeyAvailable)
            {
                ck = Console.ReadKey();
                if (ck.Key == ConsoleKey.W || ck.Key == ConsoleKey.UpArrow && cursor > 0)
                    cursor--;

                else if (ck.Key == ConsoleKey.S || ck.Key == ConsoleKey.DownArrow && cursor < 2)
                    cursor++;

                else if (ck.Key == ConsoleKey.Enter || ck.Key == ConsoleKey.Spacebar)
                {
                    switch (cursor)
                    {
                        case 0:
                            {
                                MainGame game = new MainGame();
                                game.Run();
                                break;
                            }
                        case 1:
                            {
                                OptionsMenu options = new OptionsMenu();
                                options.Run();
                                break;
                            }
                        case 2:
                            menuLoop = false;
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        private void DrawScreen()
        {
            Console.Clear();

            Console.ForegroundColor = SnakeBase.snakeColor;
            Console.SetCursorPosition(9, 4);
            Console.WriteLine("                                 88                 ");
            Console.SetCursorPosition(9, 5);
            Console.WriteLine("                                 88                 ");
            Console.SetCursorPosition(9, 6);
            Console.WriteLine("                                 88                 ");
            Console.SetCursorPosition(9, 7);
            Console.WriteLine(",adPPYba, 8b,dPPYba,  ,adPPYYba, 88   ,d8  ,adPPYba,");
            Console.SetCursorPosition(9, 8);
            Console.WriteLine("I8[    \"\" 88P'   `\"8a \"\"     `Y8 88, a8\"  a8P_____88");
            Console.SetCursorPosition(9, 9);
            Console.WriteLine(" `\"Y8ba,  88       88 ,adPPPPP88 8888[    8PP\"\"\"\"\"\"\"");
            Console.SetCursorPosition(9, 10);
            Console.WriteLine("aa    ]8I 88       88 88,    ,88 88`\"Yba, \"8b,   ,aa");
            Console.SetCursorPosition(9, 11);
            Console.WriteLine("`\"YbbdP\"' 88       88 `\"8bbdP\"Y8 88   `Y8a `\"Ybbd8\"\'");

            Console.ForegroundColor = ConsoleColor.White;
            Console.SetCursorPosition(10, 14);
            Console.WriteLine("Play");
            Console.SetCursorPosition(10, 16);
            Console.WriteLine("Options");
            Console.SetCursorPosition(10, 18);
            Console.WriteLine("Exit");

            if(File.Exists("Highscore.txt"))
            {
                FileStream fs = File.OpenRead("Highscore.txt");
                BinaryReader br = new BinaryReader(fs);
                Console.SetCursorPosition(60, 14);
                int score = br.ReadInt32();
                string player = br.ReadString();
                Console.WriteLine("Highscore: ");
                Console.SetCursorPosition(61, 15);
                Console.WriteLine(player + " - " + score);
                br.Close();
                fs.Close();
            }

            switch (cursor)
            {
                case 0:
                    Console.SetCursorPosition(15, 14);
                    break;
                case 1:
                    Console.SetCursorPosition(18, 16);
                    break;
                case 2:
                    Console.SetCursorPosition(15, 18);
                    break;
                default:
                    break;
            }
            Console.WriteLine("<-");
        }
    }
}
