﻿using System;

namespace Snake
{
    class OptionsMenu
    {
        private bool menuLoop = true;
        private int cursor = 0;
        private int gameSpeed;
        private int snakeColorNum = 0;
        private ConsoleKeyInfo ck;
        public void Run()
        {
            if (MainGame.gameSpeed == 10)
                gameSpeed = 3;
            else if (MainGame.gameSpeed == 50)
                gameSpeed = 2;
            else
                gameSpeed = 1;
            ck = new ConsoleKeyInfo();
            while (menuLoop)
            {            
                InputMannager();
                DrawScreen();
                System.Threading.Thread.Sleep(10);
            }
        }
        private void InputMannager()
        {
            if (Console.KeyAvailable)
            {
                ck = Console.ReadKey();
                if (ck.Key == ConsoleKey.W || ck.Key == ConsoleKey.UpArrow && cursor > 0)
                    cursor--;
                else if (ck.Key == ConsoleKey.S || ck.Key == ConsoleKey.DownArrow && cursor < 3)
                    cursor++;
                else if (ck.Key == ConsoleKey.D || ck.Key == ConsoleKey.RightArrow)
                {
                    switch (cursor)
                    {
                        case 0:
                            ChangeSnakeColor("forward");
                            break;
                        case 1:
                            ChangeGameSpeed("forward");
                            break;
                        case 2:
                            MainMenu.weatherModeEnable = !MainMenu.weatherModeEnable;
                            break;
                        default:
                            break;
                    }
                }
                else if (ck.Key == ConsoleKey.A || ck.Key == ConsoleKey.LeftArrow)
                {
                    switch (cursor)
                    {
                        case 0:
                            ChangeSnakeColor("backward");
                            break;
                        case 1:
                            ChangeGameSpeed("backward");
                            break;
                        case 2:
                            MainMenu.weatherModeEnable = !MainMenu.weatherModeEnable;
                            break;
                        default:
                            break;
                    }
                }
                else if (ck.Key == ConsoleKey.Enter || ck.Key == ConsoleKey.Spacebar && cursor == 3)
                    menuLoop = false;
            }
        }
        private void DrawScreen()
        {
            Console.Clear();

            Console.ForegroundColor = SnakeBase.snakeColor;
            Console.SetCursorPosition(0, 5);
            Console.WriteLine("--..,_   _,.--.   --..,_   _,.--.   --..,_   _,.--.   --..,_   _,.--.   ");
            Console.WriteLine("_. `'.:'`__ o  `;__. `'.:'`__ o  `;__. `'.:'`__ o  `;__. `'.:'`__ o  `;_");
            Console.WriteLine(" `  .'.:`. '---'`  `  .'.:`. '---'`  `  .'.:`. '---'`  `  .'.:`. '---'` ");
            Console.WriteLine("--'`.'  '.`'--....--'`.'  '.`'--....--'`.'  '.`'--....--'`.'  '.`'--....");
            Console.WriteLine("--'`      `'--....--'`      `'--....--'`      `'--....--'`      `'--....");
            Console.ForegroundColor = ConsoleColor.White;



            if (cursor == 0)
            {
                Console.SetCursorPosition(7, 14);
                Console.WriteLine("-> Color");
                Console.SetCursorPosition(17, 14);
                Console.WriteLine("<-       ->");
                Console.SetCursorPosition(20, 14);
                Console.ForegroundColor = SnakeBase.snakeColor;
                Console.WriteLine("OOOOO");
            }
            else
            {
                Console.SetCursorPosition(10, 14);
                Console.WriteLine("Color");
                Console.SetCursorPosition(20, 14);
                Console.ForegroundColor = SnakeBase.snakeColor;
                Console.WriteLine("OOOOO");
            }
            Console.ForegroundColor = ConsoleColor.White;
            if (cursor == 1)
            {
                Console.SetCursorPosition(7, 16);
                Console.WriteLine("-> Game Speed");
                Console.SetCursorPosition(22, 16);
                Console.WriteLine("<- "+gameSpeed+" ->");
            }
            else
            {
                Console.SetCursorPosition(10, 16);
                Console.WriteLine("Game Speed");
                Console.SetCursorPosition(25, 16);
                Console.WriteLine(gameSpeed);
            }
            if (cursor == 2)
            {
                Console.SetCursorPosition(7, 18);
                Console.WriteLine("-> Weather Mode");
                Console.SetCursorPosition(24, 18);
                if (MainMenu.weatherModeEnable)
                    Console.WriteLine("<- On ->");
                else
                    Console.WriteLine("<- Off ->");
            }
            else
            {
                Console.SetCursorPosition(10, 18);
                Console.WriteLine("Weather Mode");
                Console.SetCursorPosition(27, 18);
                if (MainMenu.weatherModeEnable)
                    Console.WriteLine("On");
                else
                    Console.WriteLine("Off");
            }
            if (cursor == 3)
            {
                Console.SetCursorPosition(7, 20);
                Console.WriteLine("-> Return");
            }
            else
            {
                Console.SetCursorPosition(10, 20);
                Console.WriteLine("Return");
            }
        }
        private void ChangeSnakeColor(string direction)
        {
            switch (direction)
            {
                case "forward":
                    {
                        if (snakeColorNum < 10)
                            snakeColorNum++;
                        else if (snakeColorNum == 10)
                            snakeColorNum = 0;
                        break;
                    }

                case "backward":
                    {
                        if (snakeColorNum > 0)
                            snakeColorNum--;
                        else if (snakeColorNum == 0)
                            snakeColorNum = 10;
                        break;
                    }
                default:
                    break;
            }
            switch (snakeColorNum)
            {
                case 0:
                    SnakeBase.snakeColor = ConsoleColor.Green;
                    break;
                case 1:
                    SnakeBase.snakeColor = ConsoleColor.DarkGreen;
                    break;
                case 2:
                    SnakeBase.snakeColor = ConsoleColor.DarkYellow;
                    break;
                case 3:
                    SnakeBase.snakeColor = ConsoleColor.DarkRed;
                    break;
                case 4:
                    SnakeBase.snakeColor = ConsoleColor.Magenta;
                    break;
                case 5:
                    SnakeBase.snakeColor = ConsoleColor.DarkMagenta;
                    break;
                case 6:
                    SnakeBase.snakeColor = ConsoleColor.DarkBlue;
                    break;
                case 7:
                    SnakeBase.snakeColor = ConsoleColor.Blue;
                    break;
                case 8:
                    SnakeBase.snakeColor = ConsoleColor.DarkCyan;
                    break;
                case 9:
                    SnakeBase.snakeColor = ConsoleColor.Gray;
                    break;
                case 10:
                    SnakeBase.snakeColor = ConsoleColor.White;
                    break;
                default:
                    break;
            }
        }
        private void ChangeGameSpeed(string direction)
        {
            switch (direction)
            {
                case "forward":
                    {
                        if (gameSpeed < 3)
                            gameSpeed++;
                        else if (gameSpeed == 3)
                            gameSpeed = 1;
                        break;
                    }
                case "backward":
                    {
                        if (gameSpeed > 1)
                            gameSpeed--;
                        else if (gameSpeed == 1)
                            gameSpeed = 3;
                        break;
                    }
                default:
                    break;
            }
            switch (gameSpeed)
            {
                case 1:
                    MainGame.gameSpeed = 80;
                    break;
                case 2:
                    MainGame.gameSpeed = 50;
                    break;
                case 3:
                    MainGame.gameSpeed = 10;
                    break;
                default:
                    break;
            }
        }
    }
}
