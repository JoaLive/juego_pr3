﻿using System;
using System.IO;

namespace Snake
{
    class Score
    {
        static private int score = 0;
        static private int highScore = 0;

        static public void PrintScore()
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.SetCursorPosition(2, 1);
            Console.WriteLine("Score: " + score);
        }

        static public void AddScore()
        {
            score += 1;
        }

        static public void CheckHighScore()
        {
            FileStream fs;
            if (File.Exists("Highscore.txt"))
            {
                fs = File.Open("Highscore.txt", FileMode.Open);
                BinaryReader br = new BinaryReader(fs);
                highScore = br.ReadInt32();
                br.Close();
                fs.Close();
            }

            if (score > highScore)
            {
                fs = File.Open("Highscore.txt", FileMode.OpenOrCreate);
                BinaryWriter bw = new BinaryWriter(fs);
                bw.Write(score);
                Console.WriteLine("New Highscore = " + score + "!\nEnter your name: ");
                bw.Write(Console.ReadLine());
                bw.Close();
                fs.Close();
            }
        }

        static public void ResetScore()
        {
            score = 0;
        }
        
        static public int GetScore()
        {
            return score;
        }
        
        static public void SetScore(int _score)
        {
            score = _score;
        }
    }
}
