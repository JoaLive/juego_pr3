﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Snake
{
    [Serializable]
    class SaveData
    {
        private List<SnakeBase> saveSnake;
        private List<Wall> savewalls;
        private Fruit saveFruit;
        private int saveScore;

        public SaveData(List<SnakeBase> snake, List<Wall> walls, Fruit fruit)
        {
            saveSnake = snake;
            savewalls = walls;
            saveFruit = fruit;
            saveScore = Score.GetScore();
        }

        public List<SnakeBase> GetSavedSnake()
        {
            return saveSnake;
        }

        public List<Wall> GetSavedWalls()
        {
            return savewalls;
        }

        public Fruit GetSavedFruit()
        {
            return saveFruit;
        }
        
        public int GetSavedScore()
        {
            return saveScore;
        }
    }
}
