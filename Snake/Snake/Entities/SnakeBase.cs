﻿using System;
using System.Collections.Generic;

namespace Snake
{
    [Serializable]
    class SnakeBase : DrawableObject
    {
        static public ConsoleColor snakeColor = ConsoleColor.Green;
        protected SnakeBase(int _x, int _y) : base(_x, _y)
        {
            SetColor(snakeColor);
            SetSprite('O');
        }
        public virtual void update(List<SnakeBase> snakeArr) { }
    }
}

