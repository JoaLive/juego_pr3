﻿using System;

namespace Snake
{
    [Serializable]
    class Wall : DrawableObject
    {
        public Wall(int _x, int _y) : base(_x, _y){
            SetColor(ConsoleColor.Yellow);
            SetSprite('X');
        }
        public void SetX(int _x)
        {
            X = _x;
        }
        public void SetY(int _y)
        {
            Y = _y;
        }
    }
}
