﻿using System;
using System.Collections.Generic;

namespace Snake 
{
    [Serializable]
    class Fruit : DrawableObject
    {
        private Random r;
        public Fruit(int _x, int _y) : base(_x, _y)
        {
            r = new Random();
            color = ConsoleColor.Red;
            sprite = 'X';
        }
        public void Respawn(List<Wall> walls)
        {
            Score.AddScore();
            bool respawned = false;
            int auxX;
            int auxY;
            while (!respawned)
            {
                auxX = r.Next(0, Console.WindowWidth);
                auxY = r.Next(3, Console.WindowHeight-1);
                for (int i = 0; i < walls.Count; i++)
                {
                    if (auxX == walls[i].GetX() && auxY == walls[i].GetY())
                    {
                        respawned = false;
                        break;
                    }
                    else
                    {
                        X = auxX;
                        Y = auxY;
                        respawned = true;
                    }
                }
            }         
        }
    }
}
