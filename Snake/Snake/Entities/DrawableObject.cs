﻿using System;

namespace Snake
{
    [Serializable]
    abstract class DrawableObject
    {
        protected int X;
        protected int Y;
        protected char sprite;
        protected ConsoleColor color;
        protected DrawableObject(int _x, int _y)
        {
            X = _x;
            Y = _y;
        }
        public void draw()
        {
            Console.SetCursorPosition(X, Y);
            Console.ForegroundColor = color;
            Console.Write(sprite);
        }
        public int GetX()
        {
            return X;
        }
        public int GetY()
        {
            return Y;
        }
        protected void SetColor(ConsoleColor _color)
        {
            color = _color;
        }
        protected void SetSprite(char _sprite)
        {
            sprite = _sprite;
        }
    }
}
