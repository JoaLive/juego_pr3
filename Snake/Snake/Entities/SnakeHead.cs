﻿using System;
using System.Collections.Generic;

namespace Snake
{
    [Serializable]
    class SnakeHead : SnakeBase
    {
        private ConsoleKeyInfo ck;
        private int moveX;
        private int moveY;
        public SnakeHead(int _x, int _y) : base(_x, _y)
        {
            ck = new ConsoleKeyInfo();
            moveX = 1;
            moveY = 0;
        }
        public override void update(List<SnakeBase> snakeArr)
        {
            move();
            base.draw();
        }
        public void ControlMannager(string direction)
        {
             if (direction == "left" && moveX != -1)
             {
                 moveX = 1;
                 moveY = 0;
             }
             else if (direction == "right" && moveX != 1)
             {
                 moveX = -1;
                 moveY = 0;
             }
             else if (direction == "up" && moveY != 1)
             {
                 moveX = 0;
                 moveY = -1;
             }
             else if (direction == "down" && moveY != -1)
             {
                 moveX = 0;
                 moveY = 1;
             }           
        }
        private void move()
        {
            X += moveX;
            Y += moveY;
        }
    }
}
