﻿using System;
using System.Collections.Generic;

namespace Snake
{
    [Serializable]
    class SnakeNode : SnakeBase
    {
        private int nextInLine;
        private int nextX;
        private int nextY;
        public SnakeNode(int _x, int _y, int nextNodePos, List<SnakeBase> snakeArr) : base(_x, _y)
        {
            nextInLine = nextNodePos;
            nextX = snakeArr[nextInLine].GetX();
            nextY = snakeArr[nextInLine].GetY();
        }
        public override void update(List<SnakeBase> snakeArr)
        {
            move(snakeArr);
            base.draw();
        }
        private void move(List<SnakeBase> snakeArr)
        {
            X = nextX;
            Y = nextY;
            nextX = snakeArr[nextInLine].GetX();
            nextY = snakeArr[nextInLine].GetY();
        }
    }
}
