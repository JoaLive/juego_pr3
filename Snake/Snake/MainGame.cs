﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Snake
{
    class MainGame
    {
        private Random r;

        static public int gameSpeed = 50;
        private int maxObstacles = 0;

        private bool gameLoop = true;
        private bool suspended = false;
        private ConsoleKeyInfo ck;

        private List<SnakeBase> snake;
        private Fruit fruit;
        private List<Wall> walls;

        public void Run()
        {
            r = new Random();
            ck = new ConsoleKeyInfo();
            Score.ResetScore();

            if (MainMenu.weatherModeEnable)
            {
                switch (MainMenu.weather)
                {
                    case "Sunny":
                        Console.BackgroundColor = ConsoleColor.DarkYellow;
                        break;
                    case "Cloudy":
                        Console.BackgroundColor = ConsoleColor.Gray;
                        break;
                    case "Showers":
                        Console.BackgroundColor = ConsoleColor.DarkBlue;
                        break;
                    default:
                        break;
                }
            }

            if (File.Exists("SaveData.svd"))
            {
                FileStream fs = File.OpenRead("SaveData.svd");
                BinaryFormatter bf = new BinaryFormatter();
                SaveData save = (SaveData)bf.Deserialize(fs);

                snake = save.GetSavedSnake();
                walls = save.GetSavedWalls();
                fruit = save.GetSavedFruit();
                Score.SetScore(save.GetSavedScore());

                fs.Close();
                File.Delete("SaveData.svd");
            }
            else
            {
                snake = new List<SnakeBase>();
                fruit = new Fruit(50, 10);
                walls = new List<Wall>();

                snake.Add(new SnakeHead(10, 10));
                snake.Add(new SnakeNode(9, 10, snake.Count - 1, snake));
                snake.Add(new SnakeNode(8, 10, snake.Count - 1, snake));
                snake.Add(new SnakeNode(7, 10, snake.Count - 1, snake));
                snake.Add(new SnakeNode(6, 10, snake.Count - 1, snake));
 
                PrintMap();
            }
            while (gameLoop)
            {
                Console.Clear();

                for (int i = 0; i < snake.Count; i++)
                    snake[i].update(snake);

                fruit.draw();

                for (int i = 0; i < walls.Count; i++)
                    walls[i].draw();

                CollisionsCheck();
                Score.PrintScore();

                if (Console.KeyAvailable)
                {
                    ck = Console.ReadKey();
                    if (ck.Key == ConsoleKey.Escape)
                    {
                        suspended = true;
                        SaveGame();
                        gameLoop = false;
                    }
                    else if (ck.Key == ConsoleKey.D || ck.Key == ConsoleKey.RightArrow)
                        ((SnakeHead)snake[0]).ControlMannager("left");
                    else if (ck.Key == ConsoleKey.A || ck.Key == ConsoleKey.LeftArrow)
                        ((SnakeHead)snake[0]).ControlMannager("right");
                    else if (ck.Key == ConsoleKey.W || ck.Key == ConsoleKey.UpArrow)
                        ((SnakeHead)snake[0]).ControlMannager("up");
                    else if (ck.Key == ConsoleKey.S || ck.Key == ConsoleKey.DownArrow)
                        ((SnakeHead)snake[0]).ControlMannager("down");
                }

                System.Threading.Thread.Sleep(gameSpeed);
            }
            Console.Clear();

            if (!suspended)
            {
                Console.WriteLine("Game Over");
                System.Threading.Thread.Sleep(300);
                Console.BackgroundColor = ConsoleColor.Black;
                Console.ReadKey();
                Score.CheckHighScore();              
            }
            Console.ForegroundColor = ConsoleColor.White;
        }
        private void PrintMap()
        {
            for (int i = 0; i < Console.WindowWidth-1; i++)
            {
                walls.Add(new Wall(i, 2));
                walls.Add(new Wall(i, Console.WindowHeight-1));
            }
            for (int i = 0; i < Console.WindowHeight-1; i++)
            {
                walls.Add(new Wall(0, i));
                walls.Add(new Wall(Console.WindowWidth-1, i));
            }
        }
        private void SpawnObstacles()
        {
            for (int i = 0; i < maxObstacles; i++)
            {
                walls.Add(new Wall(0, 0));
                bool spawned = false;
                int auxX;
                int auxY;
                while (!spawned)
                {
                    auxX = r.Next(0, Console.WindowWidth);
                    auxY = r.Next(3, Console.WindowHeight);
                    for (int j = 0; j < walls.Count; j++)
                    {
                        if (auxX == walls[i].GetX() && auxY == walls[i].GetY() || auxX == fruit.GetX() && auxY == fruit.GetY())
                        {
                            spawned = false;
                            break;
                        }   
                        else
                            spawned = true;                           
                    }
                    if (spawned)
                    {
                        walls[walls.Count - 1].SetX(auxX);
                        walls[walls.Count - 1].SetY(auxY);
                    }
                }
            }
        }
        private void CollisionsCheck()
        {
            if (snake[0].GetX() == fruit.GetX() && snake[0].GetY() == fruit.GetY())
            {
                snake.Add(new SnakeNode(snake[snake.Count - 1].GetX() - 1, snake[snake.Count - 1].GetY() - 1, snake.Count - 1, snake));
                SpawnObstacles();
                maxObstacles++;
                fruit.Respawn(walls);
            }
            for (int i = 0; i < snake.Count; i++)
            {
                for (int j = 1; j < snake.Count; j++)
                {
                    if (snake[i].GetX() == snake[j].GetX() && snake[i].GetY() == snake[j].GetY() && i != j)
                        gameLoop = false;
                }
                for (int k = 0; k < walls.Count; k++)
                {
                    if (snake[i].GetX() == walls[k].GetX() && snake[i].GetY() == walls[k].GetY())
                        gameLoop = false;
                }
            }
        }
        private void SaveGame()
        {
            SaveData save = new SaveData(snake, walls, fruit);

            FileStream fs = File.Open("SaveData.svd", FileMode.OpenOrCreate);
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(fs, save);

            fs.Close();
        }
    }
}